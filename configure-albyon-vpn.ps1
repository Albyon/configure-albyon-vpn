<#
.SYNOPSIS
This powershell script installs Albyon's VPN

.DESCRIPTION
The script does these tasks in the following order :
    - check for the AssumeUDPEncapsulationContextOnSendRule in the registry to avoid any NAT related issues
    - remove any existing VPN that has a Server Adress corresponding to Albyon
    - configure a new VPN with the right settings
    - Add a shortcut on the desktop to use Rasdial to connect to the VPN

.EXAMPLE
By downloading the file :
Copy the file on your C:\ drive.
Open powershell as administrator
run the command : 
. C:\configure-albyon-vpn.ps1 ; Remove-Item C:\configure-albyon-vpn.ps1 -force

By using the invoke-expression method :

#>

# Configure the registry to resolve any problem related to the NAT
$Error.clear()
$reboot = $true
$key = Get-ItemProperty -path "HKLM:\SYSTEM\CurrentControlSet\Services\PolicyAgent" -name "AssumeUDPEncapsulationContextOnSendRule"
if (-not $key) {
    Write-Output "Creating AssumeUDPEncapsulationContextOnSendRule key in registry"
    New-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Services\PolicyAgent" -Name "AssumeUDPEncapsulationContextOnSendRule" -Value 0x2 -PropertyType DWord
} elseif (-not ($key.AssumeUDPEncapsulationContextOnSendRule -eq 2)) {
    Write-Output "AssumeUDPEncapsulationContextOnSendRule was set to 2 in registry"
    Set-Itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Services\PolicyAgent" -Name "AssumeUDPEncapsulationContextOnSendRule" -Value 0x2
} else {
    Write-Output "Registry OK (no reboot required)"
    $reboot = $false
}
$Error.clear()

# Remove existing albyon VPNs
rasdial 'Albyon VPN' /disconnect | Out-Null
$vpns = Get-VpnConnection -AllUserConnection
$remove_these = @("92.154.99.243", "82.64.124.206")
foreach ($vpn in $vpns) {if ($remove_these -contains ($vpn | Select-Object -ExpandProperty ServerAddress)) {$vpn | Remove-VpnConnection -Force}}


# Ask for preshared key (the parameter -AsSecureString can be used)
# It was intentionally removed so the user can easily verify password
$psk = Read-Host 'Please enter preshared key ' -AsSecureString
$psk = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($psk)
$psk = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($psk)

# Ask for user name
$user = Read-Host "Please enter user name     "


# Add a new VPN
Add-VpnConnection -Name "Albyon VPN" `
                  -ServerAddress "92.154.99.243" `
                  -TunnelType "L2tp" `
                  -L2tpPsk $psk `
                  -AuthenticationMethod pap `
                  -EncryptionLevel NoEncryption `
                  -RememberCredential `
                  -AllUserConnection `
                  -Force


# Force encryption (Meraki's recommended settings are not allowed by default,
# hopefully it can be changed by editing the configuration file)
$conf_file = "C:\ProgramData\Microsoft\Network\Connections\Pbk\rasphone.pbk"
$config = Get-Content -path $conf_file -Raw
($config -replace 'DataEncryption=0','DataEncryption=256') | Set-Content -Path $conf_file


# Stop the program if some errors were encountered
if ($Error) {
    Write-Host "The VPN configuration failed. Please contact Albyon's IT (tristan@albyon.io)" -ForegroundColor Red
    pause
    exit
} else {
    Write-Host "The VPN was successfully configured" -ForegroundColor Green
}

# Check for public networks
$network = Get-NetConnectionProfile | Select-Object -ExpandProperty NetWorkCategory
if ($network -eq "Public") {
    $choices = '&Yes', '&No'
    $change = $Host.UI.PromptForChoice("", "You are currently on a Public Network, which can be troublesome. Switch the network type to Private ? (yes/no)?", $choices, 0)
    if ($change -eq 0) {
        Get-NetConnectionProfile | Set-NetConnectionProfile -NetworkCategory Private
    }
}

# Ask what shortcut to created
Write-Output "We will now create handy shortcuts"
$choices = '&Yes', '&No'
$remote = $Host.UI.PromptForChoice("", "Do you need to connect to a computer remotely ? (yes/no)?", $choices, 0)
if ($remote -eq 0) {$computer = Read-Host "Please enter computer name "}
$perforce = $Host.UI.PromptForChoice("", "Do you need to connect to perforce ? (yes/no)?", $choices, 0)
$services = $Host.UI.PromptForChoice("", "Do you need to reach Albyon's server and services (Gitlab, Soplanning...) ? (yes/no)?", $choices, 0)
$needed = $remote, $perforce, $services

if ($needed -contains 0) {
    # Define useful variables
    $pspath = (get-command powershell.exe).path

    # Choose location for shortcuts
    $local_user = (Get-WMIObject -class Win32_ComputerSystem | select username).username -replace '^.*\\'
    $folder = "$env:SystemDrive\Users\$local_user\desktop"
    $change = $Host.UI.PromptForChoice("", "Shortcuts will be created here : $folder. Change location ? (yes/no)?", $choices, 1)
    if ($change -eq 0) {
        [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms")|Out-Null
        $folderselect = New-Object System.Windows.Forms.FolderBrowserDialog
        $folderselect.Description = "Select a folder"
        $folderselect.rootfolder = "MyComputer"
        $folderselect.SelectedPath = $folder
        if($folderselect.ShowDialog() -eq "OK") {
            $folder = $folderselect.SelectedPath
        }
    }

    # Remote connection
    if ($remote -eq 0) {
        $arguments = @'
-WindowStyle Maximized -command "$user = 'REPLACEUSER';
Write-Host 'Welcome to Albyon. Please enter your credentials.' -ForegroundColor Green ;
$cred = Get-Credential -Message 'Enter Password' -UserName $user;
if (-not ($cred)) {Write-Host "Operation cancelled by user. Good bye" -ForegroundColor Red ; Start-Sleep -s 2 ; exit} ;
$user_full = 'REPLACEUSER@albyon.local' ;
$pw = $cred.GetNetworkCredential().Password ;
$computer_full = 'REPLACECOMPUTER.albyon.local' ;
cmdkey /generic:$computer_full /user:$user_full /pass:$pw ; 
rasdial 'Albyon VPN' $user $pw ;
Start-Process mstsc -ArgumentList "/v:$computer_full /multimon" -wait ;
rasdial 'Albyon VPN' /disconnect ;
Write-Host 'Disconnected. See you soon !' -ForegroundColor Green ;
Start-Sleep -s 2 ;
exit
'@
        $arguments = $arguments -replace "REPLACEUSER", $user
        $arguments = $arguments -replace "REPLACECOMPUTER", $computer
        $WshShell = New-Object -comObject WScript.Shell
        $Shortcut = $WshShell.CreateShortcut("$folder\Connect to $computer.lnk")
        $Shortcut.TargetPath = $pspath
        $Shortcut.Arguments = $arguments
        $Shortcut.IconLocation = "C:\Windows\System32\imageres.dll, 104"
        $Shortcut.Save()
        Write-Host "A shortcut for remoting was created on $folder\Connect to $computer.lnk" -ForegroundColor Green
    }

    # Perforce
    # Perforce is wierd : we need to open it, wait for a couple of seconds, close it, and start a new one...
    if ($perforce -eq 0) {
        $arguments = @'
-WindowStyle Maximized -command "$user = 'REPLACEUSER';
Write-Host 'Welcome to Albyon. Please enter your credentials.' -ForegroundColor Green ;
$cred = Get-Credential -Message 'Enter Password' -UserName $user;
if (-not ($cred)) {Write-Host "Operation cancelled by user. Good bye" -ForegroundColor Red ; Start-Sleep -s 2 ; exit} ;
$pw = $cred.GetNetworkCredential().Password ;
$perforce_server = 'morgana.albyon.local:1666' ;
rasdial 'Albyon VPN' $user $pw ;
$perforce_args = '-u REPLACEUSER -P REPLACEPASSWORD -p morgana.albyon.local:1666' ;
$perforce_args = $perforce_args -replace 'REPLACEPASSWORD', $pw ;
start-process P4V -WindowStyle Hidden -argumentlist $perforce_args ;
Start-Sleep -s 3 ;
Get-Process P4V* | Stop-Process ;
start-process P4V -wait ;
rasdial 'Albyon VPN' /disconnect ;
Write-Host 'Disconnected. See you soon !' -ForegroundColor Green ;
Start-Sleep -s 2 ;
exit
'@
        $arguments = $arguments -replace "REPLACEUSER", $user
        $WshShell = New-Object -comObject WScript.Shell
        $Shortcut = $WshShell.CreateShortcut("$folder\Connect to Perforce.lnk")
        $Shortcut.TargetPath = $pspath
        $Shortcut.Arguments = $arguments
        $Shortcut.IconLocation = "C:\Windows\System32\imageres.dll, 166"
        $Shortcut.Save()
        Write-Host "A shortcut for perforce was created on $folder\Connect to Perforce.lnk" -ForegroundColor Green
    }

    # Services
    if ($services -eq 0) {
        $arguments = @'
-command "$user = 'REPLACEUSER';
Write-Host 'Welcome to Albyon. Please enter your credentials.' -ForegroundColor Green ;
$cred = Get-Credential -Message 'Enter Password' -UserName $user;
if (-not ($cred)) {Write-Host "Operation cancelled by user. Good bye" -ForegroundColor Red ; Start-Sleep -s 2 ; exit} ;
$pw = $cred.GetNetworkCredential().Password ;;
rasdial 'Albyon VPN' $user $pw ;
Write-Host 'Press enter when you are done to disconnect' -ForegroundColor Green ;
Pause ;
rasdial 'Albyon VPN' /disconnect ;
Write-Host 'Disconnected. See you soon !' -ForegroundColor Green ;
Start-Sleep -s 2 ;
exit
'@
        $arguments = $arguments -replace "REPLACEUSER", $user
        $WshShell = New-Object -comObject WScript.Shell
        $Shortcut = $WshShell.CreateShortcut("$folder\Connect to Albyon VPN.lnk")
        $Shortcut.TargetPath = $pspath
        $Shortcut.Arguments = $arguments
        $Shortcut.IconLocation = "C:\Windows\System32\imageres.dll, 77"
        $Shortcut.Save()
        Write-Host "A shortcut for VPN connexion only was created on $folder\Connect to Albyon VPN.lnk" -ForegroundColor Green
    }
}


# Reboot computer if needed
if ($reboot) {
    $choices = '&Yes', '&No'
    $decision = $Host.UI.PromptForChoice($Reboot, "Your computer needs to be restarted for the changes to take effect. Reboot now (yes/no)?", $choices, 0)
    if ($decision -eq 0) {Write-Host "Rebooting now" ; Restart-Computer -Force}
}

pause
exit
