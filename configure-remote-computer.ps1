$cred = Get-Credential ;
$user = $cred.GetNetworkCredential().UserName
$user_full = "$user@albyon.local" ;
$pw = $cred.GetNetworkCredential().Password ;

$cp_full = "Albyon-ws14.albyon.local" ;
cmdkey /generic:$cp /user:$user /pass:$spw ; 
rasdial 'Albyon VPN' $user $pw ;




#Create connection shortcut
$username = (Get-WMIObject -class Win32_ComputerSystem | select username).username -replace '^.*\\'
$desktop = "$env:SystemDrive\Users\$username\desktop"
$pspath = (get-command powershell.exe).path
if (Test-Path $desktop) {
    $content = $pspath
    $args = @'
-command "$user = Read-Host 'Enter user name' ; $pw = Read-Host 'Enter password' ; $cp = Read-Host 'Enter computer name' ; rasdial 'Albyon VPN' $user $pw ; cmdkey /generic:$cp.albyon.local /user:$user@albyon.local /pass:$pw ; Start-Process mstsc /v:$cp.albyon.local -wait ; rasdial 'Albyon VPN' /disconnect
'@
    $WshShell = New-Object -comObject WScript.Shell
    $Shortcut = $WshShell.CreateShortcut("$desktop\Connect to Computer.lnk")
    $Shortcut.TargetPath = $content
    $Shortcut.Arguments = $args
    $Shortcut.Save()
    Write-Host "A shortcut was created on your desktop"
}
